const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');


const app = express();
const port = 3000;

const server = require('http').Server(app);
const io = require('socket.io')(server);

mongoose.connect('mongodb://admin:rootpass@localhost:27017/instagramClone?authSource=admin', 
    { useNewUrlParser: true }
);

app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads', 'resized')))
app.use(cors());
app.use(require('./routes'));

app.use((req, res, next) => {
    req.io = io;
    next();
})

server.listen(port, () => {
    console.log('server running')
});