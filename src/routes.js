const express = require('express');
const PostController = require('./controllers/PostController');
const likecontroller = require('./controllers/LikeController')
const uploadConfig = require('./config/multer');
const multer = require('multer');

const routes = express.Router();
const upload = multer(uploadConfig);


routes.get('/posts', PostController.findAll);
routes.post('/posts', upload.single('image'), PostController.create);
routes.post('/posts/:id', likecontroller.liked);

module.exports = routes;